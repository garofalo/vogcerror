package org.dame.vogc.error;

/**
 * @author Sabrina
 */
public class Error {
    private String code_;
    private String message_;

    public Error(String code, String message) {
        code_=code;
        message_=message;
    }

    public void setCode(String code) {
        code_=code;
    }

    public void setMessage(String message) {
        message_=message;
    }

    public String getCode() {
        return code_;
    }

    public String getMessage(){
        return message_;
    }

}
